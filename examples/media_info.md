[Go Home](./index.md)

**Get instagram media info:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

media_identifier = "CxYQJO8xuC6"
media_info: types.MediaPost = client.get_media_info(
    code_or_id_or_url=media_identifier,
    url_embed_safe=False
)

# url_embed_safe - If you need to embed images/videos on your website, set to 'true' to evade CORS restrictions.
# URLs are valid from 6 to 24 hours.
# Default: False

media_info_dict = media_info.model_dump()
```

**Returns dict (media_info_dict):**
```json
{
    "can_reshare": true,
    "can_save": true,
    "can_see_insights_as_brand": false,
    "can_view_more_preview_comments": false,
    "caption": {
      "content_type": "comment",
      "created_at": 1695138398,
      "created_at_utc": 1695138398,
      "did_report_as_spam": false,
      "id": 3195374938890887000,
      "is_covered": false,
      "is_ranked_comment": false,
      "pk": "18001437950035980",
      "private_reply_status": 0,
      "share_enabled": false,
      "status": "Active",
      "text": "“We heal and reclaim our identity through the act of creation,” says @adinasdoodles (Adina Farinango), a Kichwa artist of Ecuadorian heritage who now lives in the Indigenous territory Lenapehoking, which includes New York City. “As Indigenous creatives, this allows us to envision a future where our culture thrives and where we can authentically bring ourselves into the present and future.”\n\nAdina collaborated with fellow creative @keyraarroyo (Keyra Juliana Espinoza Arroyo), an Afro-Indigenous climate advocate and student who’s also of Ecuadorian heritage, on artwork celebrating friendship, sisterhood and their shared backgrounds. The duo first met about three years ago after connecting on Instagram. \n\n“It was relieving and warming to meet someone whose values align with mine, like Adina,” shares Keyra. “People don’t understand that art is beyond aesthetic purposes. It can also tell a story and can be a powerful tool for addressing important issues.”\n\nCheck out @design to learn more about Adina and Keyra’s work and creative process.\n\nArt by @adinasdoodles and @keyraarroyo",
      "type": 1,
      "user": {
        "account_badges": [],
        "fan_club_info": {
          "autosave_to_exclusive_highlight": null,
          "connected_member_count": null,
          "fan_club_id": null,
          "fan_club_name": null,
          "fan_consideration_page_revamp_eligiblity": null,
          "has_enough_subscribers_for_ssc": null,
          "is_fan_club_gifting_eligible": null,
          "is_fan_club_referral_eligible": null,
          "subscriber_count": null
        },
        "fbid_v2": "17841400039600391",
        "feed_post_reshare_disabled": false,
        "full_name": "Instagram",
        "has_anonymous_profile_picture": false,
        "id": "25025320",
        "is_favorite": false,
        "is_private": false,
        "is_unpublished": false,
        "is_verified": true,
        "latest_reel_media": 0,
        "profile_pic_id": "2839516949842903169_25025320",
        "profile_pic_url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-19/281440578_1088265838702675_6233856337905829714_n.jpg?stp=dst-jpg_e0_s150x150&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=1&_nc_ohc=5FiODQEf_bYAX8kljPY&edm=ALQROFkBAAAA&ccb=7-5&oh=00_AfAJlIQs9fQmDJkO1tCfTJqmL8AgV9mQqhD4W1z6A_uhyQ&oe=6525CD18&_nc_sid=fc8dfb",
        "show_account_transparency_details": true,
        "third_party_downloads_enabled": 2,
        "transparency_product_enabled": false,
        "username": "instagram"
      },
      "user_id": 25025320
    },
    "caption_is_edited": false,
    "clips_tab_pinned_user_ids": [],
    "code": "CxYQJO8xuC6",
    "comment_count": 4303,
    "comment_inform_treatment": {
      "action_type": null,
      "should_have_inform_treatment": false,
      "text": "",
      "url": null
    },
    "comment_threading_enabled": true,
    "commerciality_status": "not_commercial",
    "deleted_reason": 0,
    "device_timestamp": 169513824474824,
    "explore_hide_comments": false,
    "facepile_top_likers": [
      {
        "full_name": "MARIA  BORGES",
        "id": "259980760",
        "is_private": false,
        "is_verified": true,
        "pk": 259980760,
        "profile_pic_id": "2926926536784806237_259980760",
        "profile_pic_url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-19/306384855_1161331277794066_158078353228966493_n.jpg?stp=dst-jpg_e0_s150x150&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=1&_nc_ohc=RDoX3PJYxBgAX9jvMJf&edm=ALQROFkBAAAA&ccb=7-5&oh=00_AfAB4CoOaimjpvHLXB9q8uPu72cNFL3WfslL7byEbSfibQ&oe=65254B59&_nc_sid=fc8dfb",
        "username": "iammariaborges"
      }
    ],
    "featured_products": [],
    "filter_type": 0,
    "fundraiser_tag": {
      "has_standalone_fundraiser": false
    },
    "has_delayed_metadata": false,
    "has_liked": false,
    "has_more_comments": true,
    "has_shared_to_fb": 0,
    "hide_view_all_comment_entrypoint": false,
    "id": "3195374938890887354_25025320",
    "ig_media_sharing_disabled": false,
    "image_versions": {
      "items": [
        {
          "estimated_scans_sizes": [
            15909,
            31818,
            47727,
            63636,
            79545,
            89010,
            113269,
            128736,
            143181
          ],
          "height": 1080,
          "scans_profile": "e35",
          "url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-15/379560024_1762628070832824_3765573901785981843_n.jpg?stp=dst-jpg_e35_s1080x1080&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyIn0&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=1&_nc_ohc=E2dJoU2fiDMAX_P4N5k&edm=ALQROFkBAAAA&ccb=7-5&ig_cache_key=MzE5NTM3NDkzODg5MDg4NzM1NA%3D%3D.2-ccb7-5&oh=00_AfDBhGIwYf18-wZ4gHgq26JLssTiY-4BOg9nRL7sS1tMTA&oe=6524E181&_nc_sid=fc8dfb",
          "width": 1080
        },
        {
          "estimated_scans_sizes": [
            2834,
            5668,
            8502,
            11336,
            14170,
            17060,
            492330,
            25506,
            25506
          ],
          "height": 360,
          "scans_profile": "e35",
          "url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-15/379560024_1762628070832824_3765573901785981843_n.jpg?stp=dst-jpg_e35_s360x360&efg=eyJ2ZW5jb2RlX3RhZyI6ImltYWdlX3VybGdlbi4xNDQweDE0NDAuc2RyIn0&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=1&_nc_ohc=E2dJoU2fiDMAX_P4N5k&edm=ALQROFkBAAAA&ccb=7-5&ig_cache_key=MzE5NTM3NDkzODg5MDg4NzM1NA%3D%3D.2-ccb7-5&oh=00_AfDrdus7IVF--28IcTiGUOgUfRJRilUVJDL7YrBLkjm79Q&oe=6524E181&_nc_sid=fc8dfb",
          "width": 360
        }
      ]
    },
    "inline_composer_display_condition": "impression_trigger",
    "inline_composer_imp_trigger_time": 5,
    "integrity_review_decision": "pending",
    "is_auto_created": false,
    "is_comments_gif_composer_enabled": true,
    "is_cutout_sticker_allowed": false,
    "is_in_profile_grid": false,
    "is_open_to_public_submission": false,
    "is_organic_product_tagging_eligible": false,
    "is_paid_partnership": false,
    "is_post_live_clips_media": false,
    "is_quiet_post": false,
    "is_reshare_of_text_post_app_media_in_ig": false,
    "is_unified_video": false,
    "is_visual_reply_commenter_notice_enabled": true,
    "like_and_view_counts_disabled": false,
    "like_count": 171017,
    "mashup_info": {
      "can_toggle_mashups_allowed": false,
      "formatted_mashups_count": null,
      "has_been_mashed_up": true,
      "has_nonmimicable_additional_audio": false,
      "is_creator_requesting_mashup": false,
      "is_light_weight_check": false,
      "is_pivot_page_available": false,
      "mashup_type": null,
      "mashups_allowed": true,
      "non_privacy_filtered_mashups_media_count": 6,
      "original_media": null,
      "privacy_filtered_mashups_media_count": null
    },
    "max_num_visible_preview_comments": 2,
    "media_type": 1,
    "music_metadata": {
      "audio_type": null,
      "music_canonical_id": "0",
      "music_info": null,
      "original_sound_info": null,
      "pinned_media_ids": null
    },
    "original_height": 1440,
    "original_media_has_visual_reply_media": false,
    "original_width": 1440,
    "pk": 3195374938890887000,
    "preview_comments": [],
    "product_suggestions": [],
    "product_type": "feed",
    "profile_grid_control_enabled": false,
    "sharing_friction_info": {
      "bloks_app_url": null,
      "sharing_friction_payload": null,
      "should_have_sharing_friction": false
    },
    "shop_routing_user_id": null,
    "should_request_ads": false,
    "taken_at": 1695138398,
    "top_likers": [
      "iammariaborges"
    ],
    "user": {
      "account_badges": [],
      "fan_club_info": {
        "autosave_to_exclusive_highlight": null,
        "connected_member_count": null,
        "fan_club_id": null,
        "fan_club_name": null,
        "fan_consideration_page_revamp_eligiblity": null,
        "has_enough_subscribers_for_ssc": null,
        "is_fan_club_gifting_eligible": null,
        "is_fan_club_referral_eligible": null,
        "subscriber_count": null
      },
      "fbid_v2": "17841400039600391",
      "feed_post_reshare_disabled": false,
      "full_name": "Instagram",
      "has_anonymous_profile_picture": false,
      "id": "25025320",
      "is_favorite": false,
      "is_private": false,
      "is_unpublished": false,
      "is_verified": true,
      "latest_reel_media": 0,
      "profile_pic_id": "2839516949842903169_25025320",
      "profile_pic_url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-19/281440578_1088265838702675_6233856337905829714_n.jpg?stp=dst-jpg_e0_s150x150&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=1&_nc_ohc=5FiODQEf_bYAX8kljPY&edm=ALQROFkBAAAA&ccb=7-5&oh=00_AfAJlIQs9fQmDJkO1tCfTJqmL8AgV9mQqhD4W1z6A_uhyQ&oe=6525CD18&_nc_sid=fc8dfb",
      "show_account_transparency_details": true,
      "third_party_downloads_enabled": 2,
      "transparency_product_enabled": false,
      "username": "instagram"
    },
    "usertags": {
      "in": [
        {
          "categories": [
            "Public figure"
          ],
          "duration_in_video_in_sec": null,
          "position": [
            0.5196580936,
            0.9059828929
          ],
          "show_category_of_user": false,
          "start_time_in_video_in_sec": null,
          "user": {
            "full_name": "Keyra",
            "id": "455765941",
            "is_private": false,
            "is_verified": false,
            "profile_pic_id": "3128073450816358748_455765941",
            "profile_pic_url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-19/354603504_1924343087948469_3589713495631019442_n.jpg?stp=dst-jpg_e0_s150x150&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=109&_nc_ohc=ADJe4hvYpvAAX8cOM9W&edm=ALQROFkBAAAA&ccb=7-5&oh=00_AfAo19Ka8awTi7-U3hIt7bXjviR7hVUbm34Uhf0WeSmtyw&oe=6525B54E&_nc_sid=fc8dfb",
            "username": "keyraarroyo"
          }
        },
        {
          "categories": [
            "Artist"
          ],
          "duration_in_video_in_sec": null,
          "position": [
            0.5247863248,
            0.6752136752
          ],
          "show_category_of_user": false,
          "start_time_in_video_in_sec": null,
          "user": {
            "full_name": "Adina Farinango",
            "id": "6386594565",
            "is_private": false,
            "is_verified": false,
            "profile_pic_id": "2389495206587439503_6386594565",
            "profile_pic_url": "https://instagram.frix9-1.fna.fbcdn.net/v/t51.2885-19/118651982_159231005752636_460348549677843888_n.jpg?stp=dst-jpg_e0_s150x150&_nc_ht=instagram.frix9-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=xkhC-GUwGhMAX_Zdedr&edm=ALQROFkBAAAA&ccb=7-5&oh=00_AfCsss87JivQbW_ohdO2AKZrzzwx1ThL1hRnSFel_VSfOg&oe=65248CBA&_nc_sid=fc8dfb",
            "username": "adinasdoodles"
          }
        }
      ]
    }
}
```

---

[<<< Previous Topic (Get user followers)](./user_followers.md)

[Next Topic (Get media post comments) >>>](./media_comments.md)
