[Go Home](./index.md)

**Get media comments with pagination:**
```python
from scrapify_ig import Client, types, exceptions

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

media_identifier = "Cx8yen-uS-d"  # media code, id or url
media_comments_chunk: types.CommentsChunk = client.get_media_comments_chunk(
    code_or_id_or_url=media_identifier
)

# Also works .next_chunk interface:
if media_comments_chunk.has_next_chunk():
    next_media_comments_chunk: types.CommentsChunk = media_comments_chunk.next_chunk()


# Note:
# Comments have answers to comments. There are no answers to comments. 
# You need to call client.get_comment_thread_chunk to get answers to a specific comment.
# Example:

some_comment_with_child: types.CommentsChunkItem = media_comments_chunk.data.items[6]
child_comments_chunk: types.CommentsThreadChunk = some_comment_with_child.get_comment_thread_chunk(client)

# OR:
child_comments_chunk: types.CommentsThreadChunk = client.get_comment_thread_chunk(
    comment_id=some_comment_with_child.id
)

# Warning:
# If there is no child comment, the HTTPNotFoundError exception will be discarded
# use the .has_thread_comments method to prevent the error:

if some_comment_with_child.has_thread_comments():
    child_comments_chunk: types.CommentsThreadChunk = client.get_comment_thread_chunk(
        comment_id=some_comment_with_child.id
    )

# OR:
try:
    child_comments_chunk: types.CommentsThreadChunk = client.get_comment_thread_chunk(
        comment_id=some_comment_with_child.id
    )
except exceptions.HTTPNotFoundError:
    # do something
    pass
```

---

[<<< Previous Topic (Get media info)](./media_info.md)

[Next Topic (Get media posts by tagged user) >>>](./medias_tagged_user.md)
