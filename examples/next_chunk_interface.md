[Go Home](./index.md)

# .next_chunk interface

---

.next_chunk interface - is an interface that allows you to quickly and conveniently get the next chunk data without passing the next cursor and search parameters.
This opportunity is only available for classes that have a pagination:
```python
from scrapify_ig import types

types.FollowersChunk
types.MediaPostsChunk
types.CommentsChunk
types.CommentsThreadChunk
types.TaggedMediaPostsChunk
```
---

## Example:
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid API secret token>"

client = Client(token=TOKEN)

user_identifier = "nike"

user_medias_chunk: types.MediaPostsChunk = client.get_user_medias_chunk(
    username_or_id_or_url=user_identifier
)

next_user_medias_chunk: types.MediaPostsChunk = user_medias_chunk.next_chunk()
```

**Warning:**

But be careful! Method. next_chunk throws the exception.NoNextPageError error if it is not possible to get the next page (pagination_token is None or empty)

To avoid this, we recommend using the **.has_next_chunk()** method. This method returns True if it is possible to get the next page and does not throw an error, if the next page is not present, just returns False:

```python
if user_medias_chunk.has_next_chunk():
    next_user_medias_chunk: types.MediaPostsChunk = user_medias_chunk.next_chunk()
```

Or you can catch a error with **try/except** construction:
```python
from scrapify_ig import exceptions, types

try:
    next_user_medias_chunk: types.MediaPostsChunk = user_medias_chunk.next_chunk()
except exceptions.NoNextPageError:
    # do something
    pass
```

So you can avoid errors.

---

**Get all data with .next_chunk interface:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid API secret token>"

client = Client(token=TOKEN)

user_identifier = "nike"

first_user_medias_chunk: types.MediaPostsChunk = client.get_user_medias_chunk(
    username_or_id_or_url=user_identifier
)

if first_user_medias_chunk.has_next_chunk():
    chunk = first_user_medias_chunk.next_chunk()
    # save chunk data logic 
    while chunk.has_next_chunk():
        chunk = chunk.next_chunk()
        # save chunk data logic
```


## Alternative:
You can get the following page without using the .next_chunk interface:
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid API secret token>"

client = Client(token=TOKEN)

user_identifier = "nike"

user_medias_chunk: types.MediaPostsChunk = client.get_user_medias_chunk(
    username_or_id_or_url=user_identifier
)

next_user_medias_chunk: types.MediaPostsChunk = client.get_user_medias_chunk(
    username_or_id_or_url=user_identifier,
    pagination_token=user_medias_chunk.pagination_token
)
```

**Warning:**

In this case, you should always check the next **pagination_token**. If you transmit **pagination_token=None**, the first data page will be returned even if it is the last page on which **pagination_token=None**. So you have to make sure that this is the last page or the first page so that you don’t have an endless cycle.

---

[<<< Previous Topic (Get user medias)](./user_medias.md)

[Next Topic (Get user followers) >>>](./user_followers.md)
