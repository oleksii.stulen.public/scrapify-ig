[Go Home](./index.md)

**Get instagram user info:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

user_identifier = "nike"
user_info: types.User = client.get_user_info(
    username_or_id_or_url=user_identifier, 
    include_about=False,
    url_embed_safe=False
)

# include_about - Include 'About this account' information: country and date_joined (in about field).
# Default: False
# Note: enabling this will cost one extra request

# url_embed_safe - If you need to embed images/videos on your website, set to 'true' to evade CORS restrictions.
# URLs are valid from 6 to 24 hours.
# Default: False

user_info_dict = user_info.model_dump()
```

**Returns dict (user_info_dict):**
```json
{
    "account_badges": [],
    "account_category": "",
    "account_type": 2,
    "active_standalone_fundraisers": {
      "fundraisers": [],
      "total_count": 0
    },
    "address_street": "One Bowerman Dr",
    "ads_incentive_expiration_date": null,
    "ads_page_id": 15087023444,
    "ads_page_name": "Nike",
    "auto_expand_chaining": null,
    "avatar_status": {
      "has_avatar": false
    },
    "bio_links": [
      {
        "link_id": 17900901143536510,
        "link_type": "external",
        "lynx_url": "https://l.instagram.com/?u=https%3A%2F%2Flinkin.bio%2Fnike%3Ffbclid%3DPAAab0OJymBKm2zYPhKHXd6EPo8D9IawUcliL6IwNfJAngFDkrGQHI5huBs4c&e=AT3ndrtyC8uXODfL4VSAtYm3ZKPQyn4Z90z11G7M-HRgfABfVNDuLhvUwrt8QmCCj5Y0HhQYYfbEpYSw-Nuge-gb0ZWkzzlTZuPRbmM",
        "open_external_url_with_in_app_browser": true,
        "title": "",
        "url": "https://linkin.bio/nike"
      }
    ],
    "biography": "Spotlighting athlete* and 👟 stories\n#BlackLivesMatter, #StopAsianHate, and #NoPrideNoSport",
    "biography_with_entities": {
      "entities": [
        {
          "hashtag": {
            "id": "18001142827758664",
            "name": "nopridenosport"
          }
        },
        {
          "hashtag": {
            "id": "17843819902058362",
            "name": "stopasianhate"
          }
        },
        {
          "hashtag": {
            "id": "17841512500122531",
            "name": "blacklivesmatter"
          }
        }
      ],
      "raw_text": "Spotlighting athlete* and 👟 stories\n#BlackLivesMatter, #StopAsianHate, and #NoPrideNoSport"
    },
    "birthday_today_visibility_for_viewer": "NOT_VISIBLE",
    "business_contact_method": "UNKNOWN",
    "can_add_fb_group_link_on_profile": false,
    "can_hide_category": true,
    "can_hide_public_contacts": true,
    "can_use_affiliate_partnership_messaging_as_brand": false,
    "can_use_affiliate_partnership_messaging_as_creator": false,
    "can_use_branded_content_discovery_as_brand": false,
    "can_use_branded_content_discovery_as_creator": false,
    "category": "",
    "category_id": 0,
    "city_id": 108410602520455,
    "city_name": "Beaverton, Oregon",
    "contact_phone_number": "",
    "current_catalog_id": null,
    "direct_messaging": "UNKNOWN",
    "displayed_action_button_partner": null,
    "displayed_action_button_type": "",
    "existing_user_age_collection_enabled": true,
    "external_lynx_url": "https://l.instagram.com/?u=https%3A%2F%2Flinkin.bio%2Fnike%3Ffbclid%3DPAAab0OJymBKm2zYPhKHXd6EPo8D9IawUcliL6IwNfJAngFDkrGQHI5huBs4c&e=AT3ndrtyC8uXODfL4VSAtYm3ZKPQyn4Z90z11G7M-HRgfABfVNDuLhvUwrt8QmCCj5Y0HhQYYfbEpYSw-Nuge-gb0ZWkzzlTZuPRbmM",
    "external_url": "https://linkin.bio/nike",
    "fan_club_info": {
      "autosave_to_exclusive_highlight": null,
      "connected_member_count": null,
      "fan_club_id": null,
      "fan_club_name": null,
      "fan_consideration_page_revamp_eligiblity": null,
      "has_enough_subscribers_for_ssc": null,
      "is_fan_club_gifting_eligible": null,
      "is_fan_club_referral_eligible": null,
      "subscriber_count": null
    },
    "fb_page_call_to_action_id": "",
    "fbid_v2": "17841400602400210",
    "feed_post_reshare_disabled": false,
    "follow_friction_type": 0,
    "follower_count": 305618142,
    "following_count": 160,
    "following_tag_count": 0,
    "full_name": "Nike",
    "has_anonymous_profile_picture": false,
    "has_chaining": false,
    "has_exclusive_feed_content": false,
    "has_fan_club_subscriptions": false,
    "has_guides": true,
    "has_highlight_reels": true,
    "has_igtv_series": false,
    "has_music_on_profile": false,
    "has_private_collections": false,
    "has_public_tab_threads": true,
    "has_videos": true,
    "hd_profile_pic_url_info": {
      "height": 320,
      "url": "https://scontent-hel3-1.cdninstagram.com/v/t51.2885-19/285265415_157543166760141_7125906423211419857_n.jpg?stp=dst-jpg_e0&_nc_ht=scontent-hel3-1.cdninstagram.com&_nc_cat=1&_nc_ohc=tTLHYhT0GUgAX9fHrvi&edm=AEF8tYYBAAAA&ccb=7-5&oh=00_AfBeH2oXXiRuok9zPWxM4OlMahYw4gn3vqFR0mlPAG0LLA&oe=6524FA9E&_nc_sid=1e20d2",
      "width": 320
    },
    "hd_profile_pic_versions": [
      {
        "height": 320,
        "url": "https://scontent-hel3-1.cdninstagram.com/v/t51.2885-19/285265415_157543166760141_7125906423211419857_n.jpg?stp=dst-jpg_e0_s320x320&_nc_ht=scontent-hel3-1.cdninstagram.com&_nc_cat=1&_nc_ohc=tTLHYhT0GUgAX9fHrvi&edm=AEF8tYYBAAAA&ccb=7-5&oh=00_AfDqDHHY-K8OuP3Jy5XFEXZN8b6XSGv0-Zv-5YvqA1Q6hw&oe=6524FA9E&_nc_sid=1e20d2",
        "width": 320
      }
    ],
    "highlight_reshare_disabled": false,
    "id": "13460080",
    "include_direct_blacklist_status": true,
    "instagram_location_id": "",
    "interop_messaging_user_fbid": 113294420064920,
    "is_bestie": false,
    "is_business": true,
    "is_call_to_action_enabled": false,
    "is_category_tappable": true,
    "is_direct_roll_call_enabled": true,
    "is_eligible_for_lead_center": false,
    "is_eligible_for_smb_support_flow": true,
    "is_experienced_advertiser": false,
    "is_favorite": false,
    "is_favorite_for_clips": false,
    "is_favorite_for_highlights": false,
    "is_favorite_for_igtv": false,
    "is_favorite_for_stories": false,
    "is_in_canada": false,
    "is_interest_account": true,
    "is_memorialized": false,
    "is_new_to_instagram": false,
    "is_opal_enabled": false,
    "is_potential_business": false,
    "is_private": false,
    "is_profile_audio_call_enabled": false,
    "is_profile_broadcast_sharing_enabled": true,
    "is_profile_picture_expansion_enabled": true,
    "is_promotions_in_profile_enabled": true,
    "is_regulated_c18": false,
    "is_remix_setting_enabled_for_posts": true,
    "is_secondary_account_creation": false,
    "is_supervision_features_enabled": false,
    "is_verified": true,
    "is_whatsapp_linked": false,
    "latitude": 45.5076448,
    "lead_details_app_id": "com.bloks.www.ig.smb.lead_gen.subpage",
    "live_subscription_status": "default",
    "longitude": -122.8269159,
    "media_count": 1283,
    "mini_shop_seller_onboarding_status": null,
    "mutual_followers_count": 0,
    "nametag": null,
    "num_of_admined_pages": null,
    "open_external_url_with_in_app_browser": true,
    "page_id": 15087023444,
    "page_name": "Nike",
    "pinned_channels_info": {
      "has_public_channels": false,
      "pinned_channels_list": []
    },
    "primary_profile_link_type": 0,
    "professional_conversion_suggested_account_type": 3,
    "profile_context": "",
    "profile_context_facepile_users": [],
    "profile_context_links_with_user_ids": [],
    "profile_pic_id": "2851144256921998153_13460080",
    "profile_pic_url": "https://scontent-hel3-1.cdninstagram.com/v/t51.2885-19/285265415_157543166760141_7125906423211419857_n.jpg?stp=dst-jpg_e0_s150x150&_nc_ht=scontent-hel3-1.cdninstagram.com&_nc_cat=1&_nc_ohc=tTLHYhT0GUgAX9fHrvi&edm=AEF8tYYBAAAA&ccb=7-5&oh=00_AfB06WXdQvLzmv6CvPyKV4cR8-Ij3l6uF79mADa87_pPdg&oe=6524FA9E&_nc_sid=1e20d2",
    "profile_type": 0,
    "pronouns": [],
    "public_email": "",
    "public_phone_country_code": "",
    "public_phone_number": "",
    "recs_from_friends": {
      "enable_recs_from_friends": false,
      "recs_from_friends_entry_point_type": "banner"
    },
    "remove_message_entrypoint": false,
    "shopping_post_onboard_nux_type": null,
    "should_show_category": true,
    "should_show_public_contacts": true,
    "show_account_transparency_details": true,
    "show_fb_link_on_profile": false,
    "show_fb_page_link_on_profile": false,
    "show_ig_app_switcher_badge": true,
    "show_post_insights_entry_point": true,
    "show_text_post_app_badge": true,
    "show_text_post_app_switcher_badge": true,
    "show_together_pog": false,
    "smb_delivery_partner": null,
    "smb_support_delivery_partner": null,
    "smb_support_partner": null,
    "text_post_app_badge_label": "51,606,255",
    "text_post_app_joiner_number": 51606255,
    "text_post_app_joiner_number_label": "51,606,255",
    "third_party_downloads_enabled": 2,
    "total_ar_effects": 1,
    "total_clips_count": 1,
    "total_igtv_videos": 28,
    "transparency_product_enabled": false,
    "upcoming_events": [],
    "username": "nike",
    "whatsapp_number": "",
    "zip": "97005"
}
```

---

[Next Topic (Get user medias) >>>](./user_medias.md)
