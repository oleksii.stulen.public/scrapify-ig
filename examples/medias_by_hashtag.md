[Go Home](./index.md)

**Get Media Posts by Hashtag:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

hashtag = "nike"

hashtag_medias_chunk: types.MediaPostsHashtagChunk = client.get_medias_by_hashtag_chunk(
    hashtag=hashtag
)

# Also works .next_chunk interface:
if hashtag_medias_chunk.has_next_chunk():
    next_hashtag_medias_chunk: types.MediaPostsHashtagChunk = hashtag_medias_chunk.next_chunk()

# OR:
next_hashtag_medias_chunk: types.MediaPostsHashtagChunk = client.get_medias_by_hashtag_chunk(
    hashtag=hashtag,
    pagination_token=hashtag_medias_chunk.pagination_token
)
```

[<<< Previous Topic (Get media posts by tagged user)](./medias_tagged_user.md)

[Next Topic (Get Similar Users) >>>](./similar_users.md)
