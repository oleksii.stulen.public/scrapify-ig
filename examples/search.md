[Go Home](./index.md)

**Search Users, Hashtags and Places:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

search_query = "London"

search_result: types.SearchResult = client.search(search_query=search_query)

# Note:
# pagination not allowed
```

[<<< Previous Topic (Get Similar Users)](./similar_users.md)

[Next Topic (Get media likes) >>>](./media_likes.md)