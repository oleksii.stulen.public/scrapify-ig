[Go Home](./index.md)

**Get user followers with pagination:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

user_identifier = "nike"
user_followers_chunk: types.FollowersChunk = client.get_followers_chunk(
    username_or_id_or_url=user_identifier
)

# With .next_chunk interface:
if user_followers_chunk.has_next_chunk():
    next_user_followers_chunk: types.FollowersChunk = user_followers_chunk.next_chunk()

# OR:
next_user_followers_chunk: types.FollowersChunk = client.get_followers_chunk(
    username_or_id_or_url=user_identifier,
    pagination_token=user_followers_chunk.pagination_token
)
```

---

[<<< Previous Topic (.next_chunk interface)](./next_chunk_interface.md)

[Next Topic (Get media info) >>>](./media_info.md)
