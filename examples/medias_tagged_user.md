[Go Home](./index.md)

**Get Media Posts by Tagged user:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

user_identifier = "nike"

tagged_posts_chunk: types.TaggedMediaPostsChunk = client.get_tagged_medias_chunk(
    username_or_id_or_url=user_identifier
)

# Also works .next_chunk interface:
if tagged_posts_chunk.has_next_chunk():
    next_tagged_posts_chunk: types.TaggedMediaPostsChunk = tagged_posts_chunk.next_chunk()

# OR:
next_tagged_posts_chunk: types.TaggedMediaPostsChunk = client.get_tagged_medias_chunk(
    username_or_id_or_url=user_identifier,
    pagination_token=tagged_posts_chunk.pagination_token
)
```

[<<< Previous Topic (Get media post comments)](./media_comments.md)

[Next Topic (Get media posts by hashtag) >>>](./medias_by_hashtag.md)
