[Go Home](./index.md)

**Get Similar Users:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

user_identifier = "nike"

similar_accounts: types.SimilarAccounts = client.get_similar_accounts(
    username_or_id_or_url=user_identifier
)

# Note:
# pagination not allowed
```

[<<< Previous Topic (Get media posts by hashtag)](./medias_by_hashtag.md)

[Next Topic (Search Users, Hashtags and Places) >>>](./search.md)
