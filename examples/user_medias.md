[Go Home](./index.md)

**Get user medias with pagination:**
```python
from scrapify_ig import Client, types

TOKEN = "<Rapid api token>"

client = Client(token=TOKEN)

user_identifier = "nike"
user_medias_chunk: types.MediaPostsChunk = client.get_user_medias_chunk(
    username_or_id_or_url=user_identifier,
    url_embed_safe=False
)

# url_embed_safe - If you need to embed images/videos on your website, set to 'true' to evade CORS restrictions.
# URLs are valid from 6 to 24 hours.
# Default: False

# With .next_chunk interface:
if user_medias_chunk.has_next_chunk():
    next_user_medias_chunk: types.MediaPostsChunk = user_medias_chunk.next_chunk()

# OR:
next_user_medias_chunk: types.MediaPostsChunk = client.get_user_medias_chunk(
    username_or_id_or_url=user_identifier,
    pagination_token=user_medias_chunk.pagination_token
)
```

---

[<<< Previous Topic (Get user info)](./user_info.md)

[Next Topic (About .next_chunk interface) >>>](./next_chunk_interface.md)
