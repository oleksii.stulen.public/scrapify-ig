[Go Home](./index.md)

**Get Media Post Likes with pagination:**
```python
from scrapify_ig import Client, types

TOKEN = "secret token"
client = Client(token=TOKEN)

media_identifier = "Cx8yen-uS-d"  # media code, id or url

media_likes_chunk: types.LikesMediaPostsChunk = client.get_media_likes_chunk(
    code_or_id_or_url=media_identifier
)


# Also works .next_chunk interface:
if media_likes_chunk.has_next_chunk():
    media_likes_chunk: types.LikesMediaPostsChunk = media_likes_chunk.next_chunk()
```

[<<< Previous Topic (Search Users, Hashtags and Places)](./search.md)
